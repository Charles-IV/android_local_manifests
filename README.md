Build instructions for laurel_sprout

These builds are using the crDroid device trees. However, recently a few changes are needed.

If [this commit](https://review.lineageos.org/c/LineageOS/android_system_sepolicy/+/270271) has not been merged you will need to repopick it using `repopick 270271` after `. build/envsetup.sh`. ~~If you are using the crDroid device tree you will also need to apply [this git patch](https://gist.github.com/luk1337/284c34ca2e563636a8bbaec417b7ab16) and uncomment  `#BOARD_KERNEL_SEPARATED_DTBO := true` in BoardConfig.mk. Or you can use my fork as in the local manifest file. Thank you to LuK1337 for these fixes.~~ i think the crdroid tree now has those fixes

If you choose to use my local manifests following the instructions below, I have included my own system/sepolicy repo with the first commit merged. If you choose to repopick the commit, you will need to resolve the merge conflict, however this is not at all difficult.

Build instructions (assuming building from scratch in a new directory):
```
repo init -u https://github.com/LineageOS/android.git -b lineage-17.1
git clone https://gitlab.com/Charles-IV/android_local_manifests.git .repo/local_manifests
repo sync -c 
. build/envsetup.sh
brunch laurel_sprout
```
